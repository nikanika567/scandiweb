<?php
require_once "constants.php";

/**
 * @param $value
 * @param $type
 * @return bool
 */
function checkType($value, $type): bool
{
    if ($type == "integer") {
        return is_numeric($value);
    } else {
        return gettype($value) == $type;
    }
}

/**
 * @param $message
 * @param $code
 */
function response($message, $code) {
    header('Content-Type: application/json');
    echo json_encode(array('message' => $message, 'code' => $code));
    exit();
}