<?php

namespace app\controllers;

use app\Models\Book;
use app\Models\Dvd;
use app\Models\Furniture;

class ProductController
{

    /**
     * return all type of products
     *
     */
    public static function getProducts()
    {
        $products = [];
        $dvd = new Dvd();
        $book = new Book();
        $furniture = new Furniture();
        $products['dvds'] = $dvd->get();
        $products['furniture'] = $furniture->get();
        $products['books'] = $book->get();

        response($products, 200);
    }

    /**
     * @param array $request
     */
    public function addProduct(array $request)
    {
        $modelName = 'app\Models\\' . ucfirst($request['type']);
        if (file_exists($modelName . '.php')) {
            $model = new $modelName();
            $model->add($request);
            response('new product added', 200);
        }
        echo 'class not found';
        exit();
    }


    /**
     * delete selected products
     *
     * @param array $request
     */
    public static function deleteProducts(array $request)
    {
        $dvd = new Dvd();
        $book = new Book();
        $furniture = new Furniture();
        $dvd->delete($request);
        $book->delete($request);
        $furniture->delete($request);

        response('Selected products deleted', 200);
    }

}