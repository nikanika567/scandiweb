<?php
namespace app\Models;
require './config/helpers.php';
require_once 'Database.php';

abstract class Product
{
    protected string $sku;
    protected int $price;
    protected string $name;

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku(string $sku): void
    {
        $this->sku = $sku;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    protected Database $database;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->database = new Database();
    }

    /**
     * abstract method for get records from database for specific model
     * @return array
     */
    abstract public function get(): array;

    /**
     * abstract method for creating new record in database
     * @param array $request
     * @return mixed
     */
    abstract public function add(array $request);

    /**
     * abstract method for deleting specific records from database
     * @param array $request
     * @return mixed
     */
    abstract public function delete(array $request);

    /**
     * validate request by given validation array
     *
     * @param $request
     * @param $validation_array
     * @return bool
     */
    public function validate($request, $validation_array): bool {
        foreach ($validation_array as $name => $type) {
            if (!isset($request[$name]) || !checkType($request[$name], $type)) {
                response($name.' must be set or is not required type - '.$type, 400);
            }
        }
        $this->checkForUnique($request['sku']);

        return true;
    }

    public function checkSKU(string $sku)
    {
        $this->database->query("SELECT * FROM ". $this->table_name ." WHERE sku='$sku'");
        $result = $this->database->getResults();

        if (count($result) > 0) {
            return true;
        }
        return false;
    }

    /**
     * check if given SKU already exists in database
     *
     * @param $sku
     */
    public function checkForUnique($sku){
        $dvd = new Dvd();
        $book = new Book();
        $furniture = new Furniture();

        if ($dvd->checkSKU($sku) || $book->checkSKU($sku) || $furniture->checkSKU($sku)) {
            response('product with same "sku" ['.$sku.'] already exists', 400);
        }

    }
}