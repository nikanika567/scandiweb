<?php


namespace app\Models;
require_once 'Product.php';

class Furniture extends Product
{
    protected int $height;
    protected int $length;
    protected int $width;
    protected string $table_name = 'furniture';

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height): void
    {
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * @param int $length
     */
    public function setLength(int $length): void
    {
        $this->length = $length;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width): void
    {
        $this->width = $width;
    }

    /**
     * Dvd constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return array
     */
    public function get(): array
    {
        $this->database->query("SELECT * FROM ". $this->table_name);
        $products = $this->database->getResults();
        return $products ?? [];
    }

    /**
     * @param array $request
     */
    public function add(array $request)
    {
        $this->validate($request, $this->validation_array);
        $this->setSku($request['sku']);
        $this->setName($request['name']);
        $this->setPrice($request['price']);
        $this->setHeight($request['height']);
        $this->setWidth($request['width']);
        $this->setLength($request['length']);
        $this->database->prepare("INSERT INTO ". $this->table_name ." (sku, name, price, height, length, width) VALUES (?, ?, ?, ? ,?, ?)");
        $this->database->bind("ssiiii", [$this->getSku(), $this->getName(), $this->getPrice(), $this->getHeight(),
            $this->getLength(), $this->getWidth()]);
        $this->database->execute();
    }


    /**
     * @param array $request
     */
    public function delete(array $request)
    {
        $request = implode("', '", $request) ;
        $this->database->query("DELETE FROM ". $this->table_name ." WHERE sku in ('$request')");
    }

    /**
     * @var array
     */
    protected array $validation_array = [
        'sku' => 'string',
        'price' => 'integer',
        'name' => 'string',
        'type' => 'string',
        'height' => 'integer',
        'width' => 'integer',
        'length' => 'integer',
    ];

}