<?php


namespace app\Models;
require_once 'Product.php';

class Book extends Product
{
    protected int $weight;
    protected string $table_name = 'books';

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight(int $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * Dvd constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return array
     */
    public function get(): array
    {
        $this->database->query("SELECT * FROM ". $this->table_name);
        $products = $this->database->getResults();
        return $products ?? [];
    }

    /**
     * @param array $request
     */
    public function add(array $request)
    {
        $this->validate($request, $this->validation_array);
        $this->setSku($request['sku']);
        $this->setName($request['name']);
        $this->setPrice($request['price']);
        $this->setWeight($request['weight']);
        $this->database->prepare("INSERT INTO ". $this->table_name ." (sku, name, price, weight) VALUES (?, ?, ?, ?)");
        $this->database->bind("ssii", [$this->getSku(), $this->getName(), $this->getPrice(), $this->getWeight()]);
        $this->database->execute();
    }


    /**
     * @param array $request
     */
    public function delete(array $request)
    {
        $request = implode("', '", $request) ;
        $this->database->query("DELETE FROM ". $this->table_name ." WHERE sku in ('$request')");
    }

    /**
     * @var array
     */
    protected array $validation_array = [
        'sku' => 'string',
        'price' => 'integer',
        'name' => 'string',
        'type' => 'string',
        'weight' => 'integer'
    ];
}