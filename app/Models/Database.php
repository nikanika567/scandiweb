<?php


namespace app\Models;


class Database
{
    protected \mysqli $connection;
    protected string $error;
    protected $stmt;

    /**
     * Database constructor.
     */
    public function __construct()
    {
        try {
            $this->connection = new \mysqli(SERVER, USER, PASS, DB_NAME);
        } catch (\mysqli_sql_exception $exception) {
            $this->error = $exception->getMessage();
        }
    }

    /**
     * @param $query
     */
    public function query($query)
    {
        $this->stmt = $this->connection->query($query);
    }

    /**
     * @param $query
     */
    public function prepare($query)
    {
        $this->stmt = $this->connection->prepare($query);
    }

    /**
     * @param $type
     * @param $value
     */
    public function bind($type, $value)
    {
        $this->stmt->bind_param($type, ...$value);
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        return $this->stmt->execute();
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        $data = [];
        if ($this->stmt) {
            while($row = $this->stmt->fetch_assoc()) {
                array_push($data, $row);
            }
        }

        return $data;
    }
}