<?php


namespace app\Models;
require_once 'Product.php';

class Dvd extends Product
{
    protected int $size;
    protected string $table_name = 'dvds';

    /**
     * Dvd constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize(int $size): void
    {
        $this->size = $size;
    }

    /**
     * @return array
     */
    public function get(): array
    {
        $this->database->query("SELECT * FROM ". $this->table_name);
        $products = $this->database->getResults();
        return $products ?? [];
    }

    /**
     * @param array $request
     */
    public function add(array $request)
    {
        $this->validate($request, $this->validation_array);
        $this->setSku($request['sku']);
        $this->setName($request['name']);
        $this->setPrice($request['price']);
        $this->setSize($request['size']);
        $this->database->prepare("INSERT INTO ". $this->table_name ." (sku, name, price, size) VALUES (?, ?, ? ,?)");
        $this->database->bind("ssii", [$this->getSku(), $this->getName(), $this->getPrice(), $this->getSize()]);
        $this->database->execute();
    }

    /**
     * @param array $request
     */
    public function delete(array $request)
    {
        $request = implode("', '", $request);
        $this->database->query("DELETE FROM ". $this->table_name ." WHERE sku in ('$request')");
    }

    /**
     * @var array
     */
    protected array $validation_array = [
        'sku' => 'string',
        'price' => 'integer',
        'name' => 'string',
        'type' => 'string',
        'size' => 'integer'
    ];

}