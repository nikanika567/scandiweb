<?php

if($_SERVER['REQUEST_URI'] == '/scandiweb/') {
    header("Location:  http://127.0.0.1/scandiweb/resources/themes");
}

$route_list = [
    '/scandiweb/product/list' => ['class_name' => 'app\Controllers\ProductController', 'method' => 'getProducts'],
    '/scandiweb/product/add' => ['class_name' => 'app\Controllers\ProductController', 'method' => 'addProduct'],
    '/scandiweb/product/delete' => ['class_name' => 'app\Controllers\ProductController', 'method' => 'deleteProducts'],
];

$class_name = $route_list[$_SERVER['REQUEST_URI']]['class_name'];
$action = $route_list[$_SERVER['REQUEST_URI']]['method'];
$request_body = file_get_contents('php://input');
$data = json_decode($request_body, true);

$model = new $class_name();
$model->$action($data);









