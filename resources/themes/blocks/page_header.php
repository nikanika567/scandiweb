<header class="listing-header">
    <h3><?php echo isset($page_title) ? $page_title : 'default header' ?></h3>
    <div class="listing-header-buttons-wrap">
        <?php include "buttons_".$page.".php"; ?>
    </div>
</header>