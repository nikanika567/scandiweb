<span id="error" class="error"></span>

<form id="product_form">
    <label for="SKU">SKU</label>
    <input type="text" id="SKU" name="sku">
    <label for="name">Name</label>
    <input type="text" id="name" name="name">
    <label for="price">Price ($)</label>
    <input type="text" id="price" name="price">
    <label for="type">Select type</label>
    <select name="type" id="type">
        <option value="" selected>Type Switcher</option>
    </select>
    <div class="dynamic">

    </div>
    <span class="desc"></span>

</form>
