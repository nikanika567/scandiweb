let product = document.querySelector('.products_list')
let submit = document.querySelector('#delete')

let common_fields = [
    'sku', 'name', 'price'
]

let checked_products = []

// add product 'sku' in checked product list after checking box
let addToCheckedList = function(event)
{
    event.target.checked === true ?
        checked_products.push(event.target.value) :
        checked_products = checked_products.filter(function (val) {
            return val !== event.target.value;
        })
}

let parseDeleteResponse = function (response) {
    if (response.code === 200) {
        getProducts()
    } else {
        window.location.reload()
    }

}

// send fetch request to server after submitting delete form
let deleteProducts = async function () {
    await fetch('/scandiweb/product/delete', {
        method: 'POST',
        mode: "same-origin",
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(checked_products)
    })
        .then(response => response.json())
        .then(data => parseDeleteResponse(data))
}


if (submit != null) {
    submit.addEventListener('click', deleteProducts)
}

// helper for creating product wrapper div
let createProductDiv = function (){
    let product_div = document.createElement('div')
    product_div.className = 'product'
    return  product_div
}
// helper for creating product checker
let createChecker = function (sku) {
    let checker = document.createElement('input')
    checker.className = 'product-check'
    checker.type = 'checkbox'
    checker.value = sku
    checker.onchange = addToCheckedList
    return checker
}

let addCommonFields = function (data) {
    let div = createProductDiv()
    for (let i = 0; i < common_fields.length; i ++) {
        let element = document.createElement('span')
        let suf = common_fields[i] === 'price' ? ' $' : ''
        element.appendChild(document.createTextNode(data[common_fields[i]] + suf))
        div.appendChild(element)
    }
    return div
}

let renderdvds = function (data) {
    let div = addCommonFields(data)
    let element = document.createElement('span')
    element.appendChild(document.createTextNode('Size: ' + data['size'] + 'MB'))
    div.appendChild(element)
    div.appendChild(createChecker(data['sku']))
    product.appendChild(div)
}

let renderfurniture = function (data) {
    let div = addCommonFields(data)
    let element = document.createElement('span')
    element.appendChild(document.createTextNode('Dimension: ' + data['height'] + 'x' + data['length'] + 'x' + data['width']))
    div.appendChild(element)
    div.appendChild(createChecker(data['sku']))
    product.appendChild(div)
}

let renderbooks = function (data) {
    let div = addCommonFields(data)
    let element = document.createElement('span')
    element.appendChild(document.createTextNode('Weight: ' + data['weight'] + 'KG'))
    div.appendChild(element)
    div.appendChild(createChecker(data['sku']))
    product.appendChild(div)
}

// start products render after getting it from server
let parseResponse = function (response) {
    product.innerHTML = ""
    if (response.code === 202) {
        product.innerHTML = ""
        let status = document.querySelector("#status")
        status.appendChild(document.createTextNode(response.message))
    } else {
        for (const key in response.message) {
            for (let i = 0; i < response.message[key].length; i++)
            eval('render'+key)(response.message[key][i])
        }
    }
}

// get products list from server
let getProducts = function () {
    fetch('/scandiweb/product/list', {
        method: 'GET',
        mode: "same-origin",
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json'
        },
    })
        .then(response => response.json())
        .then(data => parseResponse(data))

}


if (product != null) {
    getProducts()
}