let validation_array = {
    common: ['sku', 'name', 'price', 'type'],
    custom: {
        dvd: ['size'],
        book: ['weight'],
        furniture: ['height', 'width', 'length']
    }
}

let product = {}

// helper function for validating form with js
let errorMessage = (field_name) => {
    let error = document.querySelector('#error')
    error.innerHTML = ''
    error.appendChild(document.createTextNode(field_name + ': is required!'))
}

// helper function for failed request to server
let serverErrorMessage = (message) => {
    let error = document.querySelector('#error')
    error.innerHTML = ''
    error.appendChild(document.createTextNode(message))
}


// check if all field are set in form
let validateForm = (form) => {
    let status = true
    for (let i = 0; i < validation_array.common.length; i++) {
        if (form[validation_array.common[i]].value === ''){
            status = false;
            errorMessage(form[validation_array.common[i]].name)
            break
        } else {
            product[form[validation_array.common[i]].name] = form[validation_array.common[i]].value
        }
    }
    if (status) {
        for (let i = 0; i < validation_array.custom[form['type'].value].length; i++){
            if (form[validation_array.custom[form['type'].value][i]].value === ''){
                errorMessage(form[validation_array.custom[form['type'].value][i]].name)
                status = false;
                break
            } else {
                product[form[validation_array.custom[form['type'].value][i]].name] = form[validation_array.custom[form['type'].value][i]].value
            }
        }
    }
    return status
}

// parse server response after submitting product form
let parseResponse = function (response) {
    if (response['code'] === 400) {
        serverErrorMessage(response['message'])
    } else if(response['code'] === 200) {
        window.location.href ='/scandiweb/resources/themes/index.php';
    }
}

// send fetch request to server after submitting product form
let addProduct = () => {
    let form = document.querySelector("#product_form");
    let type = form.type.value;
    let url = `/scandiweb/product/add`
    if(validateForm(form)) {
        fetch(url, {
            method: 'POST',
            mode: "same-origin",
            credentials: "same-origin",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(product)
        })
            .then(response => response.json())
            .then(data => parseResponse(data))
    }

}

// listen to submit event on product create
let submit = document.querySelector("#product_add")
if (submit !== null) {
    submit.addEventListener('click', addProduct)
}
