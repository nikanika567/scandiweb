let types = [
    {display_name: 'Dvd-disc', name: 'dvd', desc: 'Please, provide size', inputs: [{name: 'size', display_name: 'Size (MB)'}]},
    {display_name: 'Book', name: 'book', desc: 'Please, provide weight', inputs: [{name: 'weight', display_name: 'Weight (KG)'}]},
    {display_name: 'Furniture', name: 'furniture', desc: 'Please, provide dimensions',
        inputs: [
            {name: 'height', display_name: 'Height (CM)'},
            {name: 'width', display_name: 'Width (CM)'},
            {name: 'length', display_name: 'Length (CM)'}
        ]}
]

let selector = document.querySelector("#type")

// set description more custom fields
let setDescription = function (type)
{
    let desc = document.querySelector(".desc");
    desc.innerHTML = ''
    desc.appendChild(document.createTextNode(type.desc))
}

let active_type = '';

// watch to activeType of product and change the form base on it
let changeFormBasedActiveType = function (){
    let dynamic_area = document.querySelector(".dynamic");
    dynamic_area.innerHTML = ""
    let type = types.filter(function (value){
        return value.name === active_type
    })
    if (type[0] !== undefined) {
        for (let i = 0; i < type[0].inputs.length; i++) {
            let label = document.createElement('label')
            label.appendChild(document.createTextNode(type[0].inputs[i].display_name))
            let input = document.createElement('input')
            input.name = type[0].inputs[i].name
            dynamic_area.appendChild(label)
            dynamic_area.appendChild(input)
        }

        // set description for selected type
        setDescription(type[0])
    }

}

// change active type of product
let setActiveType = function (event) {
    active_type = event.target.value
    changeFormBasedActiveType()
}

if (selector != null) {
    selector.addEventListener('change', setActiveType)
    for (let i = 0; i < types.length; i++){
        let option = document.createElement('option')
        option.appendChild(document.createTextNode(types[i]['display_name']))
        option.value = types[i]['name']
        selector.appendChild(option)
    }
}